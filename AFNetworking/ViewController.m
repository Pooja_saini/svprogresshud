//
//  ViewController.m
//  AFNetworking
//
//  Created by click labs 115 on 11/19/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"
#import "SVProgressHUD.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *imgBack;
@property (strong, nonatomic) IBOutlet UILabel *labelMario;

@end

@implementation ViewController
@synthesize imgBack;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self performSelector:@selector(getApi) withObject:nil afterDelay:2.0];
    indicator.hidden=YES;
    
       // [self getApi];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void) getApi{
    [self showWithStatus];
    
    /*[SVProgressHUD show];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // time-consuming task
        dispatch_async(dispatch_get_main_queue(), ^{*/
            
   
    
    NSURL *url = [NSURL URLWithString:@"http://vignette2.wikia.nocookie.net/mario/images/5/5c/Jumping_Mario_Artwork_-_New_Super_Mario_Bros._Wii.png/revision/latest?cb=20120317184553"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFImageResponseSerializer serializer];
    
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
    
       // [SVProgressHUD dismiss];
    
        [SVProgressHUD dismiss];
        indicator.hidden=YES;
      imgBack.image = responseObject;
        //[self saveImage:responseObject withFilename:@"background.png"];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         [SVProgressHUD dismiss];
        NSLog(@"Error: %@", error);
    }];
    
    [operation start];
//});
//});
}
- (void)showWithStatus {
    [SVProgressHUD showWithStatus:@"loading..."];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
